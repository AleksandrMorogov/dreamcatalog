import { Injectable, Inject } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable()
export class ProductsService {
  private _req_url: string;
  private _currentId: Subject<number> = new BehaviorSubject<number>(null);
  currentId$ = this._currentId.asObservable();

  constructor(private client: HttpClient, @Inject('BASE_URL') private base_url: string) {
    this._req_url = this.base_url + 'api/Products/GetProductsByCat/';
  }
  
  GetProductsByCategory(catId: string) {
    return this.client.get<JSON>(this._req_url + catId);
  }

  SetCurrentProductId(id: number) {
    this._currentId.next(id);
  }
}
