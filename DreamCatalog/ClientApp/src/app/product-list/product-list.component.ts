import { Component, Inject, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html'
})

export class ProductListComponent {
  products;

  constructor(private route: ActivatedRoute, private productsService: ProductsService) {
    
  }
  ngOnInit() {
    let catId: string;
    this.route.params.subscribe(params => {
      catId = params['catId'];
      this.productsService.GetProductsByCategory(catId).subscribe(result => { this.products = result; });
    });
  }

  OnProductListClick(id: number) {
    this.productsService.SetCurrentProductId(id);
  }
}
