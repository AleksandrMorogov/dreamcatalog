import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<JSON>(baseUrl + 'api/SampleData/GetMenu').subscribe(result => {
      this.menu = result;
    }, error => console.error(error));
  }

  public menu: JSON;

  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
