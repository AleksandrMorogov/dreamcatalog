"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Product = /** @class */ (function () {
    function Product(name, price, count, imageId, categoryId) {
        this.name = name;
        this.price = price;
        this.count = count;
        this.imageId = imageId;
        this.categoryId = categoryId;
    }
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map