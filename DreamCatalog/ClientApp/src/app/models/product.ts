export class Product {
  constructor(
    public name: string,
    public price: number,
    public count: number,
    public imageId: number,
    public categoryId: number
  ) {}
}
