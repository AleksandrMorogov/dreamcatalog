import { Component } from '@angular/core'
import { ProductsService } from '../services/products.service';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product';

@Component({
  selector: "app-product-view",
  templateUrl: "./product-view.component.html",
})
export class ProductViewComponent {
  currentProductId: number = null;
  model: Product = null;
  constructor(private productService: ProductsService, private client: HttpClient) {
    productService.currentId$.subscribe(pid => { this.UpdateCurrentProduct(pid); })
  }
  UpdateCurrentProduct(id: number) {
    this.currentProductId = id;
    if (id == null)
      return;
    this.client.get<Product>('/api/Products/' + id.toString()).subscribe(res => {
      this.model = res;
      console.log(res);
    })
  }
  onSubmit() {
    console.log("Submit\n" + JSON.stringify(this.model));
  }
}
