﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamCatalog.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public int? Parent { get; set; }
        public string Name { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}
