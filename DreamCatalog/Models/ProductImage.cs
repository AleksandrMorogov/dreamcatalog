﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DreamCatalog.Models
{
    public class ProductImage
    {
        public int ProductImageId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
